//
//  UIColorButton.h
//  Organizze
//
//  Created by João Henrique Machado Silva on 01/09/10.
//  Copyright 2010 Mindbiz. All rights reserved.
//
//	CUSTOMIZED UIBUTTON WITH A NEW PROPERTY TO GET AND PASS RGB VALUES
//

#import <UIKit/UIKit.h>


@interface MBColorButton : UIButton {
	NSString *RGBColor;
}
@property (nonatomic, retain) NSString *RGBColor;

@end
