//
//  ColorPicker_ExampleViewController.h
//  ColorPicker Example
//
//  Created by João Henrique Machado Silva on 10/24/10.
//  Copyright MindBiz 2010. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBColorPicker.h"

@interface ColorPicker_ExampleViewController : UIViewController <MBColorPickerDelegate> {

}

@end

