//
//  UIColorButton.m
//  Organizze
//
//  Created by João Henrique Machado Silva on 01/09/10.
//  Copyright 2010 Mindbiz. All rights reserved.
//

#import "MBColorButton.h"


@implementation MBColorButton

@synthesize RGBColor;

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        // Initialization code
    }
    return self;
}

- (void)dealloc {
	[RGBColor release];
    [super dealloc];
}


@end
