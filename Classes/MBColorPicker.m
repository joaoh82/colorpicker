//
//  JHColorPicker.m
//  ColorPicker
//
//  Created by João Henrique Machado Silva on 08/09/10.
//  Copyright 2010 Mindbiz. All rights reserved.
//

#import "MBColorPicker.h"


@implementation MBColorPicker

@synthesize scrollView, cores;
@synthesize delegate;

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        // Initialization code
		[self setBackgroundColor:[UIColor clearColor]];
		
		/*
		 Building the array with about 70 different colors, all of them using alpha = 1
		 */
		cores = [[NSMutableArray alloc] initWithObjects:@"255,255,255",@"255,204,204",@"255,204,153",@"255,255,153",@"255,255,204",@"153,255,153",
								 @"153,255,255",@"204,255,255",@"204,204,255",@"255,204,255",@"204,204,204",@"255,102,102",@"255,153,102",@"255,255,102",@"255,255,51",
								 @"102,255,153",@"51,255,255",@"102,255,255",@"153,153,255",@"255,153,255",@"192,192,192",@"255,0,0",@"255,153,0",@"255,204,102",
								 @"255,255,0",@"51,255,51",@"102,204,204",@"51,204,255",@"102,102,204",@"204,102,204",@"153,153,153",@"204,0,0",@"255,102,0",@"255,204,51",
								 @"255,204,0",@"51,204,0",@"0,204,204",@"51,102,255",@"102,51,255",@"204,51,204",@"102,102,102",@"153,0,0",@"204,102,0",@"204,153,51",
								 @"153,153,0",@"0,153,0",@"51,153,153",@"51,51,255",@"102,0,204",@"153,51,153",@"51,51,51",@"102,0,0",@"153,51,0",@"153,102,51",@"102,102,0",
								 @"0,102,0",@"51,102,102",@"0,0,153",@"51,51,153",@"102,51,102",@"0,0,0",@"51,0,0",@"102,51,0",@"102,51,51",@"51,51,0",@"0,51,0",@"0,51,51",
								 @"0,0,102",@"51,0,153",@"51,0,51", nil];
		
		scrollView = [[UIScrollView alloc] initWithFrame:self.bounds];
    }
    return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
	
	[scrollView setContentSize:CGSizeMake(self.bounds.size.width, (([cores count]/5)*50)+50)];
	scrollView.clipsToBounds = YES;
	[scrollView setUserInteractionEnabled:YES];
	[scrollView setIndicatorStyle:UIScrollViewIndicatorStyleBlack];
	[scrollView setScrollEnabled:YES];
	[scrollView setBounces:YES];
	scrollView.maximumZoomScale = 4.0;
	scrollView.minimumZoomScale = 0.75;
	
	int i = 0;
	int x = 35;
	int y = 20;
	for (i; i < [cores count]; i++) {
		NSArray *listItems = [[cores objectAtIndex:i] componentsSeparatedByString:@","];
		
		NSString *red = (NSString*)[listItems objectAtIndex:0];
		NSString *green = (NSString*)[listItems objectAtIndex:1];
		NSString *blue = (NSString*)[listItems objectAtIndex:2];
		
		MBColorButton *cor = [MBColorButton buttonWithType:UIButtonTypeCustom];
		[cor setBackgroundColor:[UIColor colorWithRed:[red floatValue]/255 green:[green floatValue]/255 blue:[blue floatValue]/255 alpha:1]];
		[cor setBackgroundImage:[UIImage imageNamed:@"colorButton.png"] forState:UIControlStateNormal];
		[cor setBackgroundImage:[UIImage imageNamed:@"colorButtonClicado.png"] forState:UIControlStateHighlighted];
		[cor addTarget:self action:@selector(colorClick:) forControlEvents:UIControlEventTouchUpInside];
		cor.RGBColor = [NSString stringWithFormat:@"%@,%@,%@", red, green, blue];
		
		cor.frame = CGRectMake(x, y+20, 50, 50);
		
		[scrollView addSubview:cor];
		
		x = x + 50;
		if (x >= 270) {
			y = y + 50;
			x = 35;
		}
	}
	[self addSubview:scrollView];	
}



/*
 Method called ofter the event "Touch Up Inside" on any MBColorButton inside the component
 */
-(void)colorClick:(id)sender{
	NSString *valor = (NSMutableString*)((MBColorButton*)sender).RGBColor;
	
	NSArray *listItems = [valor componentsSeparatedByString:@","];
	
	NSString *red = (NSString*)[listItems objectAtIndex:0];
	NSString *green = (NSString*)[listItems objectAtIndex:1];
	NSString *blue = (NSString*)[listItems objectAtIndex:2];
	
	UIColor *cor = [UIColor colorWithRed:[red floatValue]/255 green:[green floatValue]/255 blue:[blue floatValue]/255 alpha:1];
	
	/*
	 Raising the delegate event, that has to be treated inside your view.
	 */
	if(self.delegate != nil)
		[[self delegate] didSelectColor:cor];
}

- (void)dealloc {
    [super dealloc];
}


@end
