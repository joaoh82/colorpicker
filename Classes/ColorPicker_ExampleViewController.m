//
//  ColorPicker_ExampleViewController.m
//  ColorPicker Example
//
//  Created by João Henrique Machado Silva on 10/24/10.
//  Copyright MindBiz 2010. All rights reserved.
//

#import "ColorPicker_ExampleViewController.h"

@implementation ColorPicker_ExampleViewController

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
	MBColorPicker *colorPicker = [[MBColorPicker alloc] initWithFrame:self.view.bounds];
	colorPicker.delegate = self;
	[self.view addSubview:colorPicker];
	
}

- (void)didSelectColor:(UIColor*)color{
	[self.view setBackgroundColor:color];
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}

@end
