//
//  ColorPicker_ExampleAppDelegate.h
//  ColorPicker Example
//
//  Created by João Henrique Machado Silva on 10/24/10.
//  Copyright MindBiz 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ColorPicker_ExampleViewController;

@interface ColorPicker_ExampleAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    ColorPicker_ExampleViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet ColorPicker_ExampleViewController *viewController;

@end

