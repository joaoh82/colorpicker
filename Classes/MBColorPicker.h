//
//  JHColorPicker.h
//  ColorPicker
//
//  Created by João Henrique Machado Silva on 08/09/10.
//  Copyright 2010 Mindbiz. All rights reserved.
//
//	
//


#import <UIKit/UIKit.h>
#import "MBColorButton.h"

@protocol MBColorPickerDelegate;

@interface MBColorPicker : UIView {
	UIScrollView *scrollView;
	NSMutableArray *cores;
	
	id <MBColorPickerDelegate> delegate;
}

@property (nonatomic, retain) UIScrollView *scrollView;
@property (nonatomic, retain) NSMutableArray *cores;

@property (nonatomic, assign) id <MBColorPickerDelegate> delegate;

@end

@protocol MBColorPickerDelegate<NSObject>

@optional

/**
 Delegate method that provide an action when the user touches on the MBColorButton with the Color and a parameter
 so you can use the color as your need
 @since version 1.0.0 
 */

- (void)didSelectColor:(UIColor*)color;

@end